package Decorator;

public class WifiDecorator implements Remote {
    protected Device device;

    public WifiDecorator() {}

    public WifiDecorator(Device device) {
        this.device = device;
    }

    @Override
    public void power() {
        System.out.println("Wifi: "+device.getName()+" power toggle");
        if (device.isEnabled()) {
            device.setVolume(25);
            device.disable();
        } else {
            device.enable();
            device.setVolume(2);
        }
    }

    @Override
    public void volumeDown() {
        if(device.isEnabled()){
            System.out.println("Wifi: "+device.getName()+" volume down");
            device.setVolume(device.getVolume()-5);
        }
        else System.out.println("Please enable device");
    }

    @Override
    public void volumeUp() {
        if(device.isEnabled()){
        System.out.println("Wifi: "+device.getName()+" volume up");
        device.setVolume(device.getVolume()*2);
        }
        else System.out.println("Please enable device");
    }

    @Override
    public void channelDown() {

        if(device.isEnabled()){
            System.out.println("Wifi: Trying to find another chanel");
            System.out.println("Wifi: Current channel is "+device.getChannel());
            if(device.getChannel()==1){
                System.out.println("Wifi: Sorry. I cannot make channel lower then 1");
            }
            else{
                System.out.println("Wifi: Found channel: "+(device.getChannel()-2));
                device.setChannel(device.getChannel()-2);
            }
        }
        else System.out.println("Please enable device");
    }

    @Override
    public void channelUp() {
        if(device.isEnabled()){
            System.out.println("Wifi: Trying to find another chanel");
            System.out.println("Wifi: Found channel: "+(device.getChannel()+5));
            device.setChannel(device.getChannel()+5);
        }
        else System.out.println("Please enable device");
    }
}
