package Decorator;

public class InfaRedDecorator implements Remote {
    protected Device device;

    public InfaRedDecorator() {}

    public InfaRedDecorator(Device device) {
        this.device = device;
    }

    @Override
    public void power() {
        System.out.println("Infared: "+device.getName()+" power toggle");
        if (device.isEnabled()) {
            device.setVolume(2);
            device.disable();
        } else {
            device.enable();
            device.setVolume(8);
        }
    }

    @Override
    public void volumeDown() {
        if(device.isEnabled()){
            System.out.println("Infared: "+device.getName()+" volume down");
            device.setVolume(device.getVolume()-3);
        }else System.out.println("Please enable device");
    }

    @Override
    public void volumeUp() {
        if(device.isEnabled()) {
            System.out.println("Infared: " + device.getName() + " volume up");
            device.setVolume(device.getVolume() * 4);
        }else System.out.println("Please enable device");
    }

    @Override
    public void channelDown() {
        if(device.isEnabled()) {
            System.out.println("Infared: Trying to find another chanel");
            System.out.println("Infared: Current channel is " + device.getChannel());
            if (device.getChannel() == 1) {
                System.out.println("Infared: Sorry. I cannot make channel lower then 1");
            } else {
                System.out.println("Infared: Found channel: " + (device.getChannel() - 1));
                device.setChannel(device.getChannel() - 1);
            }
        }
        else System.out.println("Please enable device");
    }

    @Override
    public void channelUp() {
        if(device.isEnabled()) {
            System.out.println("Infared: Trying to find another chanel");
            System.out.println("Infared: Found channel: " + (device.getChannel() + 2));
            device.setChannel(device.getChannel() + 2);
        }else System.out.println("Please enable device");
    }
}
