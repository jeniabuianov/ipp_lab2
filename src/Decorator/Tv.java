package Decorator;

public class Tv implements Device {
    private boolean on = false;
    private int volume = 30;
    private int channel = 1;
    private String name;

    Tv(String name){
        this.name = name;
    }

    @Override
    public boolean isEnabled() {
        return on;
    }

    @Override
    public void enable() {
        on = true;
    }

    @Override
    public void disable() {
        on = false;
    }

    @Override
    public int getVolume() {
        return volume;
    }

    @Override
    public void setVolume(int volume) {
        if (volume > 100) {
            this.volume = 100;
        } else if (volume < 0) {
            this.volume = 0;
        } else {
            this.volume = volume;
        }
    }

    @Override
    public int getChannel() {
        return channel;
    }

    @Override
    public void setChannel(int channel) {
        this.channel = channel;
    }

    @Override
    public String getName(){
        return name;
    }

    @Override
    public void showInfo(){
        System.out.println("******  "+this.name+"  **********");
        System.out.println("Enabled: "+(on?"true":"false"));
        System.out.println("Volume: "+volume);
        System.out.println("Channel: "+channel);
        System.out.println("**********************************");
    }

}