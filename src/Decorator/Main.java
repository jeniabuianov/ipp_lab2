package Decorator;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Please select device:");
        System.out.println("1. TV");
        System.out.println("2. Radio");

        Scanner scanner = new Scanner(System.in);
        int selected = scanner.nextInt();

        Device device = (selected==1)? new Tv("Television"): new Radio("Radio");
        WifiDecorator wifi = new WifiDecorator(device);
        InfaRedDecorator infa = new InfaRedDecorator(device);

        while(selected>0){
            System.out.println("Options:");
            System.out.println("01. Wifi volume +");
            System.out.println("02. Wifi volume -");
            System.out.println("03. Wifi chanel +");
            System.out.println("04. Wifi chanel -");
            System.out.println("05. InfaRed volume +");
            System.out.println("06. InfaRed volume -");
            System.out.println("07. InfaRed chanel +");
            System.out.println("08. InfaRed chanel -");
            System.out.println("--------------------");
            System.out.println("09. Wifi POWER ON");
            System.out.println("10. InfaRed POWER ON");
            System.out.println("--------------------");
            System.out.println("0. Exit");

            selected = scanner.nextInt();

            switch (selected){
                case 1: wifi.volumeUp(); break;
                case 2: wifi.volumeDown(); break;
                case 3: wifi.channelUp(); break;
                case 4: wifi.channelDown(); break;
                case 5: infa.volumeUp(); break;
                case 6: infa.volumeDown(); break;
                case 7: infa.channelUp(); break;
                case 8: infa.channelDown(); break;
                case 9: wifi.power(); break;
                case 10: infa.power(); break;
            }

            device.showInfo();

        }
    }
}
