package Facade;

public class Facade {
    String magazine;
    String brand;


    public void findBrand(String magazine, String brand){
        this.magazine = magazine;
        this.brand = brand;

        Magazine mg = null;
        switch (this.magazine){
            case "Atrium":
                mg = new Atrium();
                break;
            case "Jumbo":
                mg = new Jumbo();
                break;
            case "Mall":
                mg = new Mall();
                break;
        }

        if(mg.find(this.brand) != null){
            Brand br = mg.find(this.brand);
            br.showInfo();
        }
        else System.out.println("Cannot find this brand");
    }

}
