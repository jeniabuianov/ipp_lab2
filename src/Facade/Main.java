package Facade;

public class Main {

    public static void main(String[] args){
        Facade facade = new Facade();
        facade.findBrand("Atrium","MaxFashion");
        facade.findBrand("Mall","KFC");
        facade.findBrand("Jumbo","Fidesco");
    }

}
