package Facade;

public class Atrium extends Magazine {

    Atrium(){
        setName("Atrium");
        setBrands();
    }

    public void setBrands(){
        this.addBrand(new Brand("Cofee",this,1));
        this.addBrand(new Brand("Tea",this,1));
        this.addBrand(new Brand("MaxFashion",this,2));
        this.addBrand(new Brand("Plokadot",this,2));
        this.addBrand(new Brand("NLCollection",this,3));
        this.addBrand(new Brand("Serge",this,3));
        this.addBrand(new Brand("LE+OM",this,3));
    }
}
