package Facade;

public interface Shop {
    public void setNumberFloors(int number);
    public void setName(String name);
    public String getName();
    public void showInfo();
}
