package Facade;

public class Jumbo extends Magazine {

    Jumbo(){
        setName("Jumbo");
        setBrands();
    }

    public void setBrands(){
        this.addBrand(new Brand("Fidesco",this,1));
        this.addBrand(new Brand("Maib",this,1));
        this.addBrand(new Brand("Orange",this,2));
        this.addBrand(new Brand("Moldcell",this,2));
        this.addBrand(new Brand("Kare",this,3));
        this.addBrand(new Brand("Step",this,3));
        this.addBrand(new Brand("CofeeBeans",this,3));
    }
}
