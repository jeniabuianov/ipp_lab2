package Facade;

public class Brand {
    String name;
    Magazine magazine;
    int floor;


    Brand(String name, Magazine magazine, int floor){
        this.name = name;
        this.magazine = magazine;
        this.floor = floor;
    }

    public void showInfo(){
        System.out.println(name+" ->> "+floor+" floor "+magazine.getName());
    }

    public String getName() {
        return name;
    }
}
