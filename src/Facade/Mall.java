package Facade;

public class Mall extends Magazine {

    Mall(){
        setName("Mall");
        setBrands();
    }

    public void setBrands(){
        this.addBrand(new Brand("KFC",this,1));
        this.addBrand(new Brand("McDonald's",this,1));
        this.addBrand(new Brand("Andy's Pizza",this,2));
        this.addBrand(new Brand("Beef House",this,2));
        this.addBrand(new Brand("Blinoff",this,3));
        this.addBrand(new Brand("Star Kebab",this,3));
        this.addBrand(new Brand("Panifico",this,3));
    }
}
