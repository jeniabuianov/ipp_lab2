package Facade;

import java.util.ArrayList;
import java.util.List;

public abstract class Magazine implements Shop{
    int numberFloors;
    String name;
    List<Brand> brands = new ArrayList<>();

    @Override
    public void setNumberFloors(int number) {
        this.numberFloors = number;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void showInfo(){
        System.out.println("**********************");
        System.out.println("Name: "+name);
        System.out.println("Floors: "+numberFloors);

        System.out.println("**********************");
    }

    public void addBrand(Brand brand){
        this.brands.add(brand);
    }

    public Brand find(String b){
        for (Brand brand:brands) {
            if (brand.getName().equals(b))
                return brand;
        }
        return null;
    }
}
