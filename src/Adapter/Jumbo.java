package Adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Jumbo extends Magazine{

    private double width;
    private double height;

    Jumbo(double width, double height){
        this.setName("Jumbo");
        this.setNumberFloors(3);
        this.setNumberOfButOnLevel(5);
        this.setDefaultBrands();
        this.width = width;
        this.height = height;
    }

    private void setDefaultBrands(){
        HashMap<Integer,List<String>> brands = new HashMap<>();
        List<String> firstFloor = new ArrayList<>();
        firstFloor.add("Maib");
        firstFloor.add("Fidesco");
        firstFloor.add("Librarius");
        firstFloor.add("Orange");
        firstFloor.add("Moldcell");


        List<String> secondFloor = new ArrayList<>();
        secondFloor.add("Sex Shop");

        brands.put(1,firstFloor);
        brands.put(2,secondFloor);
        this.setBrands(brands);
    }

    public double getArea(){
        return (width*height)*0.95 * this.getNumberFloors();
    }

    public double getWidth(){
        return width;
    }

    public double getHeight(){
        return height;
    }
}
