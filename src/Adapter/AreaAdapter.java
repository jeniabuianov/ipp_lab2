package Adapter;

import java.util.ArrayList;
import java.util.List;

public class AreaAdapter {
    List<Magazine> magazines = new ArrayList<>();
    public void add(Magazine magazine){
        magazines.add(magazine);
    }

    public boolean canResize(int m1, int m2){
        m1--;
        m2--;

        double elat = magazines.get(m2).getWidth() -5;
        double jumbo = magazines.get(m2).getWidth()*0.95;
        double w = (jumbo>elat)?elat:jumbo;


        if (magazines.get(m1).getWidth() < w){
            elat = magazines.get(m2).getHeight() -1.5;
            jumbo = magazines.get(m2).getWidth()*0.95;
            double h = (jumbo>elat)?elat:jumbo;

            if(magazines.get(m1).getHeight()<h)
                return true;

        }
        return false;
    }
}
