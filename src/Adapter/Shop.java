package Adapter;

public interface Shop {
    public void setNumberFloors(int numberFloors);
    public void setName(String name);
    public void setBrand(String brand);
    public int findBrandByName(String name);
    public void setNumberOfButOnLevel(int but);
    public void showBrands();
    public String getName();
}
