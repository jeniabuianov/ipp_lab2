package Adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Magazine implements Shop{
    private int numberFloors = 0;
    private int numberOfButOnLevel = 0;
    private String magazineName = "";
    private HashMap<Integer,List<String>> brands = new HashMap<>();

    @Override
    public String getName(){
        return this.magazineName;
    }

    @Override
    public void setNumberOfButOnLevel(int but) {
        numberOfButOnLevel = but;
    }

    @Override
    public void setNumberFloors(int numberFloors) {
        this.numberFloors = numberFloors;
    }

    @Override
    public void setName(String name) {
        this.magazineName = name;
    }

    @Override
    public void setBrand(String brand) {
        if (brands.size()<=numberFloors && numberOfButOnLevel<brands.get(brands.size()-1).size()){
            brands.get(brands.size()-1).add(brand);
        }
        else System.out.println("Sorry, we can't add new butic, cause all places are busy");
    }

    @Override
    public int findBrandByName(String name) {
        for (Integer i:brands.keySet()){
            for (int j=0;j<brands.get(i).size();j++){
                if (brands.get(i).get(j).toLowerCase().equals(name.trim().toLowerCase())) {
                    return i;
                }
            }
        }
        return 0;
    }

    public void setBrands(HashMap<Integer,List<String>> brands){
        this.brands = brands;
    }


    public int getNumberFloors(){
        return numberFloors;
    }

    @Override
    public void showBrands(){
        for (Integer i:brands.keySet()){
            for (int j=0;j<brands.get(i).size();j++)
                System.out.println(brands.get(i).get(j));
        }

    }

    public abstract double getArea();
    public abstract double getWidth();
    public abstract double getHeight();

}
