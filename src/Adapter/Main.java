package Adapter;

public class Main {

    public static void main(String[] args){
        Magazine atrium = new Atrium(80,35);
        Magazine jumbo = new Jumbo(30,18);
        Magazine elat = new Elat(60,10);

        AreaAdapter areaAdapter = new AreaAdapter();
        areaAdapter.add(atrium);
        areaAdapter.add(jumbo);
        areaAdapter.add(elat);

        for (int i=1;i<=areaAdapter.magazines.size();i++){
            for (int j=1;j<=areaAdapter.magazines.size();j++){
                if (i!=j){
                    if (areaAdapter.canResize(i,j)){
                        System.out.println(areaAdapter.magazines.get(i-1).getName()+" can be resized to "+areaAdapter.magazines.get(j-1).getName());
                    }
                    else
                        System.out.println(areaAdapter.magazines.get(i-1).getName()+" cannot be resized to "+areaAdapter.magazines.get(j-1).getName());
                }
            }
        }
    }
}
