package Adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Elat extends Magazine {
    private double width;
    private double height;

    Elat(double width, double height){
        this.setName("Elat");
        this.setNumberFloors(3);
        this.setNumberOfButOnLevel(2);
        this.setDefaultBrands();
        this.width = width;
        this.height = height;
    }

    private void setDefaultBrands(){
        HashMap<Integer,List<String>> brands = new HashMap<>();
        List<String> firstFloor = new ArrayList<>();
        firstFloor.add("Zorile");
        firstFloor.add("Suschimania");

        List<String> secondFloor = new ArrayList<>();
        secondFloor.add("MaxFashion");

        brands.put(1,firstFloor);
        brands.put(2,secondFloor);
        this.setBrands(brands);
    }

    public double getArea(){
        return (width-5)*(height-1.5)* this.getNumberFloors();
    }

    public double getWidth(){
        return width;
    }

    public double getHeight(){
        return height;
    }
}
