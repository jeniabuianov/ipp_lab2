package Adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Atrium extends Magazine{

    private double width;
    private double height;

    Atrium(double width, double height){
        this.setName("Atrium");
        this.setNumberFloors(5);
        this.setNumberOfButOnLevel(3);
        this.setDefaultBrands();
        this.width = width;
        this.height = height;
    }

    private void setDefaultBrands(){
        HashMap<Integer,List<String>> brands = new HashMap<>();
        List<String> firstFloor = new ArrayList<>();
        firstFloor.add("CoffeeBeans");
        firstFloor.add("Vizaj Nica");
        firstFloor.add("Librarius");

        List<String> secondFloor = new ArrayList<>();
        secondFloor.add("Icos");
        secondFloor.add("Esigarete");
        secondFloor.add("Luminare");

        List<String> thierdFloor = new ArrayList<>();
        thierdFloor.add("Bulgarian Rose");

        brands.put(1,firstFloor);
        brands.put(2,secondFloor);
        brands.put(3,thierdFloor);
        this.setBrands(brands);
    }

    public double getArea(){
        return width*height*this.getNumberFloors();
    }

    public double getWidth(){
        return width;
    }

    public double getHeight(){
        return height;
    }
}
